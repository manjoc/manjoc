const path = require("path")
module.exports = {
  components: "src/features/**/[A-Z]*.tsx",
  getExampleFilename(componentPath) {
    return componentPath.replace("features", "docs").replace(".tsx", ".md")
  },
  styleguideComponents: {
    Wrapper: path.join(__dirname, "src/components/Provider.tsx"),
  },
  template: {
    head: {
      links: [
        {
          rel: "stylesheet",
          href: "https://unpkg.com/leaflet@1.7.1/dist/leaflet.css",
        },
      ],
    },
  },
}
