import React from "react"

class Stores {}

const storesContext = React.createContext<Stores | null>(null)

export const StoresProvider: React.FC = ({children}) => {
  const stores = new Stores()
  return (
    <storesContext.Provider value={stores}>{children}</storesContext.Provider>
  )
}

export const useStores = () => {
  const stores = React.useContext(storesContext)
  if (!stores) throw new Error("Missing StoresProvider")

  return stores
}
