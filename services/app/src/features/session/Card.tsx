import React from "react"

import {
  Avatar,
  AvatarBadge,
  AvatarGroup,
  Heading,
  StackProps,
  Text,
  Tooltip,
  VStack,
  Wrap,
  WrapItem,
} from "@chakra-ui/react"
import Sugar from "sugar"

import Feed from "../../components/messages/Feed"
import Map from "../../components/Map"
import MotionVStack from "../../components/motion/MotionVStack"
import Session from "../../types/Session"

interface CardProps extends StackProps {
  session: Session
  mini?: boolean
}

const Card = ({mini, session, ...rest}: CardProps) => {
  const dateTime = Sugar.Date(session.date)

  return (
    <MotionVStack
      alignItems="stretch"
      borderWidth={mini && "1px"}
      overflow="hidden"
      p={3}
      rounded="md"
      spacing={3}
      whileHover={mini && {scale: 1.02}}
      whileTap={mini && {scale: 0.98}}
      {...rest}
    >
      <VStack alignItems="flex-start" spacing={0}>
        <Heading size="sm">{session.crag.name.toUpperCase()}</Heading>
        <Text fontSize="xs" color="grey.500">
          {dateTime.medium().raw}
        </Text>
      </VStack>

      <Map
        readOnly
        location={session.crag.location}
        maxH={mini ? "40" : "auto"}
        ratio={[5 / 3]}
        rounded="md"
      />

      <Wrap>
        <Tooltip label={session.owner?.fullName}>
          <WrapItem>
            <Avatar
              name={session.owner?.fullName}
              showBorder
              size="sm"
              src={session.owner?.avatar}
            >
              <AvatarBadge bg="green.500" boxSize="0.8em" />
            </Avatar>
          </WrapItem>
        </Tooltip>

        {session.participants && (
          <WrapItem>
            <AvatarGroup max={3} size="sm" spacing={-2}>
              {session.participants.map(participant => (
                <Avatar
                  key={participant.id}
                  name={participant.fullName}
                  src={participant.avatar}
                />
              ))}
            </AvatarGroup>
          </WrapItem>
        )}
      </Wrap>

      {!mini && session.messages && <Feed session={session} />}

      {/* TODO: Add action buttons */}
    </MotionVStack>
  )
}

export default Card
