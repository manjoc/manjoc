import React from "react"

import {
  FormControl,
  FormErrorMessage,
  FormHelperText,
  FormLabel,
  Input,
  VStack,
} from "@chakra-ui/react"
import {FormProvider, useForm} from "react-hook-form"
import {useTranslation} from "react-i18next"

import DateTimeInput from "../../components/form/DateTimeInput"
import LocatorInput from "../../components/form/LocatorInput"
import Session from "../../types/Session"
import UserSelectorInput from "../../components/form/UserSelectorInput"

interface FormProps {
  session?: Session
}

const Form = (props: FormProps) => {
  const {session} = props
  const {t} = useTranslation()

  const methods = useForm({mode: "onTouched"})
  const {formState} = methods

  return (
    <FormProvider {...methods}>
      <VStack
        alignItems="stretch"
        as="form"
        borderWidth="1px"
        maxW="3xl"
        overflow="hidden"
        rounded="lg"
        spacing={6}
        p={3}
      >
        <FormControl isInvalid={formState.errors.locator} isRequired>
          <FormLabel>{t("form.where.label")}</FormLabel>

          {/* TODO: Raise an error if the markerLocation is not defined */}
          <LocatorInput
            borderWidth="1px"
            borderColor="gray.200"
            location={session?.crag.location}
            overflow="hidden"
            rounded="md"
            style={{
              WebkitMaskImage: "-webkit-radial-gradient(white, black)",
            }}
          />

          <FormHelperText display="flex" mb={3}>
            {t("form.where.help")}
          </FormHelperText>

          <FormErrorMessage>
            {formState.errors.locator && formState.errors.locator.message}
          </FormErrorMessage>
        </FormControl>

        <FormControl isRequired>
          {/* TODO: use react-hook-form */}
          <FormLabel>{t("form.crag.label")}</FormLabel>

          <Input
            defaultValue={session?.crag.name}
            placeholder={t("form.crag.placeholder")}
          />
        </FormControl>

        <FormControl isInvalid={formState.errors.dateTime} isRequired>
          <FormLabel>{t("form.when.label")}</FormLabel>

          {/* TODO: Pre-fill the defaultValue on edit mode */}
          <DateTimeInput name="dateTime" isRequired mustBeLater />

          <FormErrorMessage>
            {formState.errors.dateTime && formState.errors.dateTime.message}
          </FormErrorMessage>
        </FormControl>

        {/* Participants */}
        <FormControl>
          <FormLabel>{t("form.participants.label")}</FormLabel>
          <UserSelectorInput participants={session?.participants} />
          <FormHelperText>{t("form.participants.help")}</FormHelperText>
        </FormControl>
      </VStack>
    </FormProvider>
  )
}

export default Form
