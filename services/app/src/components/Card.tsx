import React from "react"

import {Divider, StackProps, VStack} from "@chakra-ui/react"

const Card = (props: StackProps) => (
  <VStack
    alignItems="stretch"
    bg="gray.50"
    borderColor="gray.200"
    borderWidth="1px"
    divider={<Divider variant="dashed" />}
    p={3}
    rounded="md"
    spacing={3}
    w="100%"
    {...props}
  >
    {props.children}
  </VStack>
)

export default Card
