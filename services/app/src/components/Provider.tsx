import React from "react"
import {ChakraProvider, ChakraProviderProps, theme} from "@chakra-ui/react"

import "../locales/i18n"

require("sugar/locales")

export const Provider = (props: ChakraProviderProps) => (
  <ChakraProvider theme={theme}>{props.children}</ChakraProvider>
)

export default Provider
