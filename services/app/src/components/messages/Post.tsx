import React, {Dispatch} from "react"

import {
  Avatar,
  AvatarBadge,
  ButtonGroup,
  HStack,
  Icon,
  IconButton,
  Link,
  StackProps,
  Text,
  Tooltip,
  VStack,
} from "@chakra-ui/react"
import {FiCornerDownRight, FiCornerUpLeft, FiMoreVertical} from "react-icons/fi"
import {useTranslation} from "react-i18next"
import Linkify from "react-linkify"
import Sugar from "sugar"

import Message from "../../types/Message"
import Card from "../Card"

interface ThreadProps extends StackProps {
  message: Message
  ownerId?: string
  setReplyTo: Dispatch<Message>
}

const Parent = (props: ThreadProps) => {
  const {message, ownerId, setReplyTo, ...rest} = props

  return (
    <Card {...rest}>
      <Post
        key={message.id}
        isOwner={message.user?.id === ownerId}
        message={message}
        parent={message}
        setReplyTo={setReplyTo}
      />
      {message.children?.map(child => (
        <HStack spacing={3} w="100%">
          <Icon as={FiCornerDownRight} color="gray.300" boxSize="0.8em" />
          <Post
            key={child.id}
            isOwner={child.user?.id === ownerId}
            message={child}
            parent={message}
          />
        </HStack>
      ))}
    </Card>
  )
}

interface PostProps {
  isOwner?: boolean
  message: Message
  parent: Message
  setReplyTo?: Dispatch<Message>
}

const Post = (props: PostProps) => {
  const {isOwner, message, setReplyTo} = props
  const {t} = useTranslation()

  return (
    <VStack alignItems="stretch" spacing={1} w="100%">
      <HStack justifyContent="space-between">
        <HStack>
          <Avatar
            borderColor="gray.50"
            name={message.user?.fullName}
            showBorder
            size="sm"
            src={message.user?.avatar}
          >
            {isOwner && <AvatarBadge boxSize="0.8em" bg="green.400" />}
          </Avatar>

          <VStack alignItems="flex-start" spacing={0}>
            <Text fontSize="sm" fontWeight="bold">
              {message.user?.fullName}
            </Text>
            <Text color="gray.600" fontSize="sm">
              {Sugar.Date(message.createdAt).relative().raw}
            </Text>
          </VStack>
        </HStack>
        <ButtonGroup isAttached size="sm" variant="outline">
          {setReplyTo && (
            <Tooltip label={t("messages.actions.reply.tooltip")}>
              <IconButton
                aria-label="reply"
                icon={<FiCornerUpLeft />}
                mr="-px"
                onClick={() => {
                  setReplyTo(message)
                }}
              />
            </Tooltip>
          )}
          <Tooltip label={t("messages.actions.edit.tooltip")}>
            <IconButton aria-label="edit" icon={<FiMoreVertical />} />
          </Tooltip>
        </ButtonGroup>
      </HStack>
      <Text color="gray.600">
        <Linkify
          componentDecorator={url => (
            <Link color="teal.500" href={url} isExternal>
              {url}
            </Link>
          )}
        >
          {message.content}
        </Linkify>
      </Text>
    </VStack>
  )
}

export default Parent
