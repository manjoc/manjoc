import React, {useEffect, useState} from "react"

import {
  Button,
  CloseButton,
  Heading,
  HStack,
  StackProps,
  Text,
  Textarea,
  VStack,
} from "@chakra-ui/react"
import {useTranslation} from "react-i18next"
import {FiCheck} from "react-icons/fi"

import Post from "./Post"
import Message from "../../types/Message"
import Session from "../../types/Session"
import Card from "../Card"
import {useRef} from "react"

interface FeedProps extends StackProps {
  session: Session
}

export const Feed = ({session, ...rest}: FeedProps) => {
  const {t} = useTranslation()
  const [newMessage, setNewMessage] = useState("")
  const [replyTo, setReplyTo] = useState<Message>()
  const messageTextArea = useRef<null | HTMLTextAreaElement>(null)

  useEffect(() => {
    replyTo && messageTextArea.current?.focus()
  }, [replyTo])

  return (
    <VStack alignItems="stretch" p={3} spacing={4} {...rest}>
      <Heading size="lg">{t("messages.label")}</Heading>
      {session.messages?.map(message => (
        <Post
          key={message.id}
          message={message}
          ownerId={session.owner?.id}
          setReplyTo={setReplyTo}
        />
      ))}
      <Card p={0} spacing={0}>
        {replyTo && (
          <HStack justifyContent="space-between" mb="-px" p={3} w="100%">
            <VStack alignItems="flex-start" fontSize="sm" spacing={0}>
              <Text
                dangerouslySetInnerHTML={{
                  __html: t("messages.actions.reply.label", {
                    fullName: replyTo.user?.fullName,
                  }),
                }}
              />
              <Text color="gray.500" fontSize="xs" noOfLines={1}>
                {replyTo.content}
              </Text>
            </VStack>
            <CloseButton
              bg="gray.100"
              onClick={() => {
                setReplyTo(undefined)
              }}
              rounded="full"
              size="sm"
            />
          </HStack>
        )}
        <Textarea
          border="none"
          placeholder={t("messages.actions.submit.placeholder")}
          onChange={e => setNewMessage(e.target.value)}
          ref={messageTextArea}
          resize="vertical"
          roundedTop={replyTo && "none"}
          value={newMessage}
        />
      </Card>
      <HStack>
        <Button colorScheme="teal" rightIcon={<FiCheck />}>
          {t("messages.actions.submit.label")}
        </Button>
        <Button
          onClick={() => {
            setNewMessage("")
            setReplyTo(undefined)
          }}
          textColor="gray.500"
        >
          {t("messages.actions.cancel.label")}
        </Button>
      </HStack>
    </VStack>
  )
}

export default Feed
