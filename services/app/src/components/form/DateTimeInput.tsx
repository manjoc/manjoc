import React from "react"

import {Input, InputProps} from "@chakra-ui/react"
import {useFormContext} from "react-hook-form"
import {useTranslation} from "react-i18next"
import Sugar from "sugar"

interface DateTimeInputProps extends InputProps {
  mustBeLater?: boolean
  name: string
}

const dateTimeFormat: string = "%x %X"

const DateTimeInput = (props: DateTimeInputProps) => {
  const {formState, getValues, register, setValue} = useFormContext()
  const {t} = useTranslation()

  // Try to create a localized Date from the provided value
  function createSugarDate(value: string): sugarjs.Date.Chainable<Date> {
    return Sugar.Date(value, {locale: navigator.language})
  }

  return (
    <Input
      {...register(props.name, {
        onBlur: e => {
          if (
            !formState.errors.dateTime &&
            e.target.type !== "datetime-local"
          ) {
            setValue(props.name, getValues(props.name).format(dateTimeFormat))
          }
        },

        required: {
          message: t("form.when.errors.required"),
          value: props.isRequired ?? false,
        },

        validate: {
          isValid: value =>
            value.isValid().raw || t("form.when.errors.format")!,

          isFuture: value =>
            (props.mustBeLater && value.isFuture().raw) ||
            t("form.when.errors.past")!,
        },

        setValueAs: value => createSugarDate(value),
      })}
      type="datetime-local"
    />
  )
}

export default DateTimeInput
