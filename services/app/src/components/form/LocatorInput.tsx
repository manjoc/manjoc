import React, {useEffect, useState} from "react"

import axios from "axios"
import {
  HStack,
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
  StackProps,
  Text,
  useToast,
  VStack,
} from "@chakra-ui/react"
import {useFormContext} from "react-hook-form"
import {useTranslation} from "react-i18next"
import {FiChevronLeft, FiChevronRight, FiSearch} from "react-icons/fi"

import Map from "../Map"
import {
  Candidate,
  FindPlacesRequestParams,
  FindPlacesResponse,
} from "../../types/places"
import {Location} from "../../types/Crag"

interface LocatorInputProps extends StackProps {
  location?: Location
}

const apiURL: string = "http://localhost:8080" // TODO: use environment variable

const LocatorInput = ({location, ...rest}: LocatorInputProps) => {
  const {getValues, register, setValue} = useFormContext()
  const toast = useToast()
  const {t} = useTranslation()

  let [focusIndex, setFocus] = useState(0)
  const [isSearching, setIsSearching] = useState(false)

  const [currentLocation, setCurrentLocation] = useState<Location>()
  const [places, setPlaces] = useState<Candidate[]>([])

  function centerMap(place: Candidate) {
    setCurrentLocation(place.geometry.location)
    setValue("locator", place.formatted_address)
  }

  useEffect(() => {
    setCurrentLocation(location)
  }, [location])

  async function search() {
    setIsSearching(true)

    setFocus(0)

    try {
      const params: FindPlacesRequestParams = {
        input: getValues("locator"),
      }

      const res = await axios.get<FindPlacesResponse>(`${apiURL}/places`, {
        params,
      })

      const candidates: Candidate[] = res.data.candidates
      setPlaces(candidates)

      if (candidates.length > 0) centerMap(candidates[0])
    } catch {
      toast({
        description: t("form.where.errors.server"),
        status: "error",
      })
    }

    setIsSearching(false)
  }

  function updateFocusIndex(action: "down" | "up") {
    switch (action) {
      case "down":
        focusIndex = focusIndex === 0 ? places.length - 1 : focusIndex - 1
        break

      case "up":
        focusIndex = focusIndex === places.length - 1 ? 0 : focusIndex + 1
        break
    }

    setFocus(focusIndex)
    centerMap(places[focusIndex])
  }

  return (
    <VStack alignItems="stretch" spacing={0} {...rest}>
      <Map location={currentLocation} ratio={[1 / 1, 5 / 2]} />

      {/* The controls */}
      <VStack p={3}>
        {/* The search bar */}
        <InputGroup>
          <Input
            onKeyPress={e => e.key === "Enter" && search()}
            placeholder={t("form.where.placeholder")}
            {...register("locator")}
            variant="filled"
          />
          <InputRightElement onClick={() => search()}>
            <IconButton
              aria-label="search"
              icon={<FiSearch />}
              isLoading={isSearching}
              variant="link"
            />
          </InputRightElement>
        </InputGroup>

        {/* The focusIndex selector */}
        {places.length > 1 && (
          <HStack>
            <IconButton
              aria-label="previous"
              icon={<FiChevronLeft />}
              onClick={() => updateFocusIndex("down")}
              size="lg"
              variant="link"
            />
            <Text color="gray.500">
              {focusIndex + 1}/{places.length}
            </Text>
            <IconButton
              aria-label="next"
              icon={<FiChevronRight />}
              onClick={() => updateFocusIndex("up")}
              size="lg"
              variant="link"
            />
          </HStack>
        )}
      </VStack>
    </VStack>
  )
}

export default LocatorInput
