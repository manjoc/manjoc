import React, {useEffect, useState} from "react"

import axios from "axios"
import {
  Avatar,
  HStack,
  Input,
  StackProps,
  Text,
  Tooltip,
  useToast,
  VStack,
} from "@chakra-ui/react"
import {debounce, pullAllWith, without} from "lodash"
import {useFormContext} from "react-hook-form"
import {useTranslation} from "react-i18next"

import User from "../../types/User"

const apiURL: string = "http://localhost:8080" // TODO: use environment variable

interface UserSelectorInputProps extends StackProps {
  participants?: User[]
}

interface SearchUserParams {
  search: string
}

const UserSelectorInput = (props: UserSelectorInputProps) => {
  const {getValues, register, setValue} = useFormContext()
  const toast = useToast()
  const {t} = useTranslation()

  const [deletionConfirmed, setDeletionConfirmed] = useState<boolean>(false)
  const [participants, setParticipants] = useState<User[]>([])
  const [usersFound, setUsersFound] = useState<User[]>([])

  function addParticipant(user: User) {
    toast.closeAll()

    participants.unshift(user)

    setParticipants(participants)

    setUsersFound([])
    setValue("search-users", null)

    toast({
      description: t("form.participants.added", {
        fullName: user.fullName,
      }),
      duration: 1000,
      status: "success",
    })
  }

  function removeParticipant(participant: User) {
    toast.closeAll()

    if (deletionConfirmed) {
      setParticipants(without(participants, participant))
      setDeletionConfirmed(false)

      toast({
        description: t("form.participants.removed", {
          fullName: participant.fullName,
        }),
        duration: 1000,
        status: "success",
      })
    } else {
      setDeletionConfirmed(true)
    }
  }

  const searchUsers = debounce(async () => {
    try {
      const value: string = getValues("search-users")

      if (value === "") {
        setUsersFound([])
        return
      }

      const params: SearchUserParams = {
        search: value,
      }

      const res = await axios.get(`${apiURL}/users`, {params})

      setUsersFound(
        pullAllWith(
          res.data,
          participants,
          (userFound, participant) => userFound.id === participant.id
        )
      )
    } catch {
      toast.closeAll()

      toast({
        description: t("form.participants.errors.server"),
        status: "error",
      })
    }
  }, 1000)

  useEffect(() => {
    if (props.participants) setParticipants(props.participants)
  }, [props.participants])

  return (
    <VStack alignItems="flex-start">
      <Input
        placeholder={t("form.participants.placeholder")}
        {...register("search-users", {
          onChange: searchUsers,
        })}
      />

      <HStack overflow="auto" w="100%">
        {usersFound?.map(userFound => (
          <Tooltip
            hasArrow
            key={userFound.id}
            label={t("form.participants.add", {
              fullName: userFound.fullName,
            })}
            openDelay={200}
            placement="top"
          >
            <VStack
              borderWidth="1px"
              borderColor="transparent"
              cursor="pointer"
              onClick={() => addParticipant(userFound)}
              p={3}
              rounded="md"
              _hover={{
                backgroundColor: "blue.50",
                borderColor: "blue.200",
              }}
            >
              <Avatar name={userFound.fullName} src={userFound.avatar} />
              <Text fontSize="xs" maxW="28" isTruncated>
                {userFound.fullName}
              </Text>
            </VStack>
          </Tooltip>
        ))}

        {participants?.map(participant => {
          return (
            <Tooltip
              closeOnClick={deletionConfirmed ? true : false}
              hasArrow
              key={participant.id}
              label={
                deletionConfirmed
                  ? t("form.participants.confirm")
                  : t("form.participants.remove", {
                      fullName: participant.fullName,
                    })
              }
              onClose={() => setDeletionConfirmed(false)}
              openDelay={deletionConfirmed ? 0 : 200}
              placement="top"
            >
              <VStack
                backgroundColor="gray.50"
                borderWidth="1px"
                borderColor="gray.200"
                cursor="pointer"
                onClick={() => removeParticipant(participant)}
                p={3}
                rounded="md"
                _hover={{
                  backgroundColor: "blue.50",
                  borderColor: "blue.200",
                }}
              >
                <Avatar name={participant.fullName} src={participant.avatar} />
                <Text fontSize="xs" maxW="28" isTruncated>
                  {participant.fullName}
                </Text>
              </VStack>
            </Tooltip>
          )
        })}
      </HStack>
    </VStack>
  )
}

export default UserSelectorInput
