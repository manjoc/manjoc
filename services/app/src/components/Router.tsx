import React from "react"

import {BrowserRouter, Route} from "react-router-dom"

import Welcome from "../pages/Welcome"

const Router = () => (
  <BrowserRouter>
    <Route path="/">
      <Route path="welcome" element={<Welcome />} />
    </Route>
  </BrowserRouter>
)

export default Router
