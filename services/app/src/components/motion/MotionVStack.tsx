import React from "react"

import {forwardRef, VStack} from "@chakra-ui/react"
import {isValidMotionProp, motion} from "framer-motion"

import {MotionProps} from "../../types/MotionProps"

const MotionVStack = motion(
  forwardRef<MotionProps, typeof VStack>((props, ref) => {
    const chakraProps = Object.fromEntries(
      Object.entries(props).filter(([key]) => !isValidMotionProp(key))
    )
    return <VStack ref={ref} {...chakraProps} />
  })
)

export default MotionVStack
