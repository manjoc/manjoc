import React from "react"

import {forwardRef, HStack} from "@chakra-ui/react"
import {isValidMotionProp, motion} from "framer-motion"

import {MotionProps} from "../../types/MotionProps"

const MotionHStack = motion(
  forwardRef<MotionProps, typeof HStack>((props, ref) => {
    const chakraProps = Object.fromEntries(
      Object.entries(props).filter(([key]) => !isValidMotionProp(key))
    )
    return <HStack ref={ref} {...chakraProps} />
  })
)

export default MotionHStack
