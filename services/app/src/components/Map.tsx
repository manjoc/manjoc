import React, {useEffect, useState} from "react"

import {AspectRatio, BoxProps, Center, Link} from "@chakra-ui/react"
import {Map as LeafletMap} from "leaflet"
import {MapContainer, Marker, TileLayer} from "react-leaflet"

import {Location} from "../types/Crag"

interface MapProps extends BoxProps {
  location?: Location
  ratio: number[]
  readOnly?: boolean
}

const mapDefaults = {
  zoom: 13,
  location: {
    lat: 48.7758,
    lng: 9.1829,
  },
}

const Map = ({location, ratio, readOnly, ...rest}: MapProps) => {
  const [map, setMap] = useState<LeafletMap>()
  const [marker, setMarker] = useState<Location>()

  useEffect(() => {
    if (location) {
      map?.setView(location, mapDefaults.zoom)

      if (!readOnly) {
        map?.addEventListener("click", (event: any) =>
          setMarker(map.mouseEventToLatLng(event.originalEvent))
        )
      }

      setMarker(location)
    }
  }, [location, map, readOnly])

  return (
    <Center
      overflow="hidden"
      position="relative"
      style={{
        WebkitMaskImage: "-webkit-radial-gradient(white, black)",
      }}
      {...rest}
    >
      <AspectRatio ratio={ratio} w="100%" zIndex={1}>
        {/* The map */}
        <MapContainer
          attributionControl={false}
          center={mapDefaults.location}
          doubleClickZoom={!readOnly}
          dragging={!readOnly}
          scrollWheelZoom={!readOnly}
          whenCreated={setMap}
          zoom={4}
          zoomControl={!readOnly}
        >
          <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

          {marker && (
            <Marker
              draggable={!readOnly}
              eventHandlers={{
                dragend: event => setMarker(event.target._latlng),
              }}
              interactive={!readOnly}
              position={marker}
            />
          )}
        </MapContainer>
      </AspectRatio>

      {/* The OSM copyright link */}
      <Link
        backgroundColor="gray.500"
        color="white"
        fontSize="xs"
        href="http://osm.org/copyright"
        isExternal
        position="absolute"
        px={1}
        right={0}
        roundedBottomLeft="md"
        top={0}
        zIndex={2}
      >
        &copy; OpenStreetMap
      </Link>
    </Center>
  )
}

export default Map
