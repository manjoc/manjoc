import React from "react"

import {Button, Center, Heading, Text, VStack} from "@chakra-ui/react"
import {useTranslation} from "react-i18next"
import {FiArrowRight} from "react-icons/fi"
import {Link} from "react-router-dom"

const Welcome = () => {
  const {t} = useTranslation()

  return (
    <Center h="100vh">
      <VStack maxW="lg" p={3} spacing={12}>
        <Heading
          fontWeight="extrabold"
          textAlign="center"
          size="4xl"
          textColor="gray.700"
        >
          {t("welcome.title")}
          <Text color="teal.500">{t("appName")}</Text>
        </Heading>

        <Text color="gray.500" fontSize="xl" textAlign="center">
          {t("welcome.info")}
        </Text>

        <Button
          as={Link}
          colorScheme="teal"
          rightIcon={<FiArrowRight />}
          size="lg"
          to="#"
        >
          {t("welcome.actions.getStarted")}
        </Button>
      </VStack>
    </Center>
  )
}

export default Welcome
