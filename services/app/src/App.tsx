import React from "react"

import Provider from "./components/Provider"
import Router from "./components/Router"
import {StoresProvider} from "./stores"

export const App = () => (
  <Provider>
    <StoresProvider>
      <Router />
    </StoresProvider>
  </Provider>
)
