### Create a session

```js
<Form />
```

### Edit an existing session

```js
<Form
  session={{
    id: "e66a7e96-d4a0-4153-a828-2272bf7e5999",
    date: "2021-01-01",
    crag: {
      name: "Troubat's Crag",
      location: {
        lat: 42.975681,
        lng: 0.58079,
      },
    },
  }}
/>
```
