### Session Card

```js
<Card
  maxW="lg"
  session={{
    id: "73cb5d8c-5935-4ac0-96f5-9df654e3a5e6",
    date: "2021-01-01",
    crag: {
      name: "Troubat's Crag",
      location: {
        lat: 42.975681,
        lng: 0.58079,
      },
    },
    messages: [
      {
        createdAt: "2021-04-01",
        content: "Beware, rocks on the road!",
        user: {
          avatar: "https://bit.ly/dan-abramov",
          fullName: "Dan Abrahmov",
        },
      },
    ],
  }}
/>
```

### Mini Session Card

```js
<Card
  maxW="sm"
  mini
  session={{
    id: "788cafbf-2165-48c1-81a7-fd5ba76e2e12",
    date: "2021-01-01",
    crag: {
      name: "Troubat's Crag",
      location: {
        lat: 42.975681,
        lng: 0.58079,
      },
    },
  }}
/>
```
