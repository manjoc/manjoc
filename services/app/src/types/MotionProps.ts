import {ChakraProps} from "@chakra-ui/react"
import {MotionProps as FramerMotionProps} from "framer-motion"

// See: https://codesandbox.io/s/chakra-ui-motion-box-sbk9e
export type MotionProps = Omit<ChakraProps, keyof FramerMotionProps> &
  FramerMotionProps & {as?: React.ElementType}
