import Crag from "./Crag"
import Message from "./Message"
import Model from "./Model"
import User from "./User"

export default interface Session extends Model {
  date: string
  private?: boolean
  crag: Crag
  owner?: User
  messages?: Message[]
  participants?: User[]
}
