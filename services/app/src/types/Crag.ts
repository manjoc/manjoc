import Model from "./Model"
import Session from "./Session"

export default interface Crag extends Model {
  name: string
  location: Location
  topo?: string
  sessions?: Session[]
}

export interface Location {
  lat: number
  lng: number
}
