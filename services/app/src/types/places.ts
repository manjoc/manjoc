import {Location} from "./Crag"

export interface FindPlacesRequestParams {
  input: string
}

export interface FindPlacesResponse {
  candidates: []
  status: string
}

export interface Candidate {
  formatted_address: string
  geometry: {location: Location}
  name: string
}
