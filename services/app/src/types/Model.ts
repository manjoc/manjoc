export default interface Model {
  id: string
  createdAt: string
  updatedAt: string
}
