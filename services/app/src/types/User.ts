import Message from "./Message"
import Model from "./Model"
import Session from "./Session"

export default interface User extends Model {
  avatar?: string
  eightA?: string
  email: string
  fullName: string
  messages?: Message[]
  sessions?: Session[]
}
