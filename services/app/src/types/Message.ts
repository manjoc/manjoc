import Model from "./Model"
import Session from "./Session"
import User from "./User"

export default interface Message extends Model {
  content: string
  session?: Session
  user?: User
  children?: Message[]
}
