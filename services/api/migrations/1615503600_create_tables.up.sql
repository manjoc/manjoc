BEGIN;

CREATE TABLE crags (
  id UUID PRIMARY KEY,
  name TEXT,
  slug TEXT,
  lat REAL,
  lng REAL,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE UNIQUE INDEX idx_crags ON crags(slug, lat, lng);

CREATE TABLE sessions (
  id UUID PRIMARY KEY,
  date TIMESTAMP WITH TIME ZONE,
  private BOOLEAN,
  crag_id UUID REFERENCES crags ON DELETE CASCADE,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE users (
  id UUID PRIMARY KEY,
  avatar TEXT,
  eight_a TEXT,
  email TEXT,
  full_name TEXT,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE UNIQUE INDEX idx_users ON users(email);

CREATE TYPE participation_status AS ENUM('unknown', 'owner', 'accepted', 'refused');

CREATE TABLE participations (
  session_id UUID REFERENCES sessions ON DELETE CASCADE,
  user_id UUID REFERENCES users ON DELETE CASCADE,
  status participation_status DEFAULT 'unknown',
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  PRIMARY KEY (session_id, user_id)
);

CREATE TABLE messages (
  id UUID PRIMARY KEY,
  content TEXT,
  parent_id UUID REFERENCES messages ON DELETE CASCADE,
  session_id UUID REFERENCES sessions ON DELETE CASCADE,
  user_id UUID REFERENCES users ON DELETE CASCADE,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

COMMIT;
