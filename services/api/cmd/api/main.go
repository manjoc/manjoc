package main

import (
	"api/google"
	"api/mux"
	"api/pq"
	"net/http"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/patrickmn/go-cache"
)

const (
	CacheCleanupInterval = 10 * time.Minute // Remove expired items every 10 minutes
	CacheDefaultDuration = 1 * time.Hour    // Items expire after a hour

	// ServerTimeout is timeout for HTTP requests.
	ServerTimeout = 30 * time.Second
)

func main() {
	// Load environment file (ignore errors as it is only used in development)
	_ = godotenv.Load()

	// Initialize in-memory cache engine
	cache := cache.New(CacheDefaultDuration, CacheCleanupInterval)

	// Open database connection
	db, err := pq.Open(&pq.Config{
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		Database: os.Getenv("DB_NAME"),
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
	})
	if err != nil {
		panic(err)
	}

	// Create router with services
	router := mux.NewRouter(
		pq.NewCrags(db),
		pq.NewMessages(db),
		pq.NewParticipations(db),
		google.NewPlaces(
			cache,
			os.Getenv("GOOGLE_PLACES_API_KEY"),
			os.Getenv("GOOGLE_PLACES_API_URL"),
		),
		pq.NewSessions(db),
		pq.NewUsers(db),
	)

	// Create and start server
	s := &http.Server{
		Handler:      router.Handler(),
		Addr:         os.Getenv("API_HOST"),
		WriteTimeout: ServerTimeout,
		ReadTimeout:  ServerTimeout,
	}
	s.ListenAndServe()
}
