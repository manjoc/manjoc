package api

import (
	"net/mail"
)

// User represents end users of the application.
type User struct {
	Model
	Avatar         string           `json:"avatar,omitempty"`
	EightA         string           `json:"eightA,omitempty"`
	Email          string           `json:"email,omitempty"`
	FullName       string           `json:"fullName,omitempty"`
	Participations []*Participation `json:"participations,omitempty"`
}

// Validate performs basic formatting and validation.
func (u *User) Validate() error {
	if _, err := mail.ParseAddress(u.Email); err != nil {
		return err
	}

	return nil
}

// UserFilter represents a filter used to find users.
type UserFilter struct {
	ID       string `column:"id"`
	Email    string `column:"email"`
	FullName string `column:"full_name"`
	Limit    int
	Offset   int
}

// UserUpdates represents a set of fields that can be updated for a user.
type UserUpdates struct {
	Avatar   *string `json:"avatar"`
	EightA   *string `json:"eightA"`
	Email    *string `json:"email"`
	FullName *string `json:"fullName"`
}

// Users represents a service to manage users.
type Users interface {
	Find(filter *UserFilter) ([]*User, int, error)
	FindByID(id string) (*User, error)
	Create(user *User) error
	Update(id string, u *UserUpdates) (*User, error)
	Delete(id string) error
}
