package api

import (
	"errors"
)

// Crag represents a climbing crag. It provides information such as name and
// coordinates.
type Crag struct {
	Model
	Name     string     `json:"name,omitempty"`
	Slug     string     `json:"slug,omitempty"`
	Lat      float64    `json:"lat,omitempty"`
	Lng      float64    `json:"lng,omitempty"`
	Sessions []*Session `json:"sessions,omitempty"`
}

// Validate performs basic formatting and validation.
func (c *Crag) Validate() error {
	c.Slug = c.Name
	if err := NormalizeString(&c.Slug); err != nil {
		return err
	}

	if c.Lat == 0 || c.Lng == 0 {
		return errors.New("Invalid coordinates")
	}

	return nil
}

// CragFilter represents a filter used to find crags.
type CragFilter struct {
	ID     string `column:"id"`
	Name   string `column:"name"`
	Limit  int
	Offset int
}

// CragUpdates represents a set of fields that can be updated for a crag.
type CragUpdates struct {
	Name *string  `json:"name"`
	Lat  *float64 `json:"lat"`
	Lng  *float64 `json:"lng"`
}

// Crags represents a service to manage crags.
type Crags interface {
	Find(filter *CragFilter) ([]*Crag, int, error)
	FindByID(id string) (*Crag, error)
	Create(crag *Crag) error
	Update(id string, u *CragUpdates) (*Crag, error)
	Delete(id string) error
}
