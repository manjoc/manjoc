package mux

import (
	"api"
	"api/pq"
	"errors"
	"net/http"
)

func (r *router) handleMessages() {
	r.mux.HandleFunc("/messages", r.findMessages).Methods("GET")
	r.mux.HandleFunc("/messages/{id}", r.findMessageByID).Methods("GET")
	r.mux.HandleFunc("/messages", r.createMessage).Methods("POST")
	r.mux.HandleFunc("/messages/{id}", r.updateMessage).Methods("PATCH")
	r.mux.HandleFunc("/messages/{id}", r.deleteMessage).Methods("DELETE")
}

func (r *router) findMessages(w http.ResponseWriter, req *http.Request) {
	messages, n, err := r.Messages.Find(&api.MessageFilter{
		Offset: Offset(req),
		Limit:  PaginationLimit,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, messages, Header{"X-Total", n})
}

func (r *router) findMessageByID(w http.ResponseWriter, req *http.Request) {
	message, err := r.Messages.FindByID(GetVar(req, "id"))
	if err != nil {
		if errors.Is(err, pq.ErrRecordNotFound) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		return
	}

	WriteJSON(w, message)
}

func (r *router) createMessage(w http.ResponseWriter, req *http.Request) {
	message := &api.Message{}
	if err := ReadJSON(req, message); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := r.Messages.Create(message); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, message)
}

func (r *router) updateMessage(w http.ResponseWriter, req *http.Request) {
	u := &api.MessageUpdates{}
	if err := ReadJSON(req, u); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	message, err := r.Messages.Update(GetVar(req, "id"), u)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, message)
}

func (r *router) deleteMessage(w http.ResponseWriter, req *http.Request) {
	if err := r.Messages.Delete(GetVar(req, "id")); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
