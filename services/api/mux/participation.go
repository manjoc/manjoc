package mux

import (
	"api"
	"api/pq"
	"errors"
	"net/http"
)

func (r *router) handleParticipations() {
	r.mux.HandleFunc("/participations", r.findParticipations).Methods("GET")
	r.mux.HandleFunc("/sessions/{sessionID}/users/{userID}/participations", r.findParticipation).Methods("GET")
	r.mux.HandleFunc("/participations", r.createParticipation).Methods("POST")
	r.mux.HandleFunc("/sessions/{sessionID}/users/{userID}/participations", r.updateParticipation).Methods("PATCH")
	r.mux.HandleFunc("/sessions/{sessionID}/users/{userID}/participations", r.deleteParticipation).Methods("DELETE")
}

func (r *router) findParticipations(w http.ResponseWriter, req *http.Request) {
	participations, n, err := r.Participations.Find(&api.ParticipationFilter{
		Offset: Offset(req),
		Limit:  PaginationLimit,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, participations, Header{"X-Total", n})
}

func (r *router) findParticipation(w http.ResponseWriter, req *http.Request) {
	participation, err := r.Participations.FindByIDs(
		GetVar(req, "sessionID"),
		GetVar(req, "userID"),
	)
	if err != nil {
		if errors.Is(err, pq.ErrRecordNotFound) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		return
	}

	WriteJSON(w, participation)
}

func (r *router) createParticipation(w http.ResponseWriter, req *http.Request) {
	participation := &api.Participation{}
	if err := ReadJSON(req, participation); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := r.Participations.Create(participation); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, participation)
}

func (r *router) updateParticipation(w http.ResponseWriter, req *http.Request) {
	u := &api.ParticipationUpdates{}
	if err := ReadJSON(req, u); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	participation, err := r.Participations.Update(
		GetVar(req, "sessionID"), GetVar(req, "userID"), u,
	)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, participation)
}

func (r *router) deleteParticipation(w http.ResponseWriter, req *http.Request) {
	if err := r.Participations.Delete(
		GetVar(req, "sessionID"), GetVar(req, "userID"),
	); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
