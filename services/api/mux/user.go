package mux

import (
	"api"
	"api/pq"
	"errors"
	"net/http"
)

func (r *router) handleUsers() {
	r.mux.HandleFunc("/users", r.findUsers).Methods("GET")
	r.mux.HandleFunc("/users/{id}", r.findUserByID).Methods("GET")
	r.mux.HandleFunc("/users", r.createUser).Methods("POST")
	r.mux.HandleFunc("/users/{id}", r.updateUser).Methods("PATCH")
	r.mux.HandleFunc("/users/{id}", r.deleteUser).Methods("DELETE")
}

func (r *router) findUsers(w http.ResponseWriter, req *http.Request) {
	users, n, err := r.Users.Find(&api.UserFilter{
		Email:    req.URL.Query().Get("email"),
		FullName: req.URL.Query().Get("fullName"),
		Limit:    PaginationLimit,
		Offset:   Offset(req),
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, users, Header{"X-Total", n})
}

func (r *router) findUserByID(w http.ResponseWriter, req *http.Request) {
	user, err := r.Users.FindByID(GetVar(req, "id"))
	if err != nil {
		if errors.Is(err, pq.ErrRecordNotFound) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		return
	}

	WriteJSON(w, user)
}

func (r *router) createUser(w http.ResponseWriter, req *http.Request) {
	user := &api.User{}
	if err := ReadJSON(req, user); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := r.Users.Create(user); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, user)
}

func (r *router) updateUser(w http.ResponseWriter, req *http.Request) {
	u := &api.UserUpdates{}
	if err := ReadJSON(req, u); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user, err := r.Users.Update(GetVar(req, "id"), u)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, user)
}

func (r *router) deleteUser(w http.ResponseWriter, req *http.Request) {
	if err := r.Users.Delete(GetVar(req, "id")); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
