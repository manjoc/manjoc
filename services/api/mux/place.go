package mux

import (
	"net/http"
)

func (r *router) handlePlaces() {
	r.mux.HandleFunc("/places", r.findPlaces).Methods("GET")
}

func (r *router) findPlaces(w http.ResponseWriter, req *http.Request) {
	places, err := r.Places.Find(
		req.URL.Query().Get("input"),
	)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, places)
}
