package mux

import (
	"api"
	"api/pq"
	"errors"
	"net/http"
)

func (r *router) handleCrags() {
	r.mux.HandleFunc("/crags", r.findCrags).Methods("GET")
	r.mux.HandleFunc("/crags/{id}", r.findCragByID).Methods("GET")
	r.mux.HandleFunc("/crags", r.createCrag).Methods("POST")
	r.mux.HandleFunc("/crags/{id}", r.updateCrag).Methods("PATCH")
	r.mux.HandleFunc("/crags/{id}", r.deleteCrag).Methods("DELETE")
}

func (r *router) findCrags(w http.ResponseWriter, req *http.Request) {
	crags, n, err := r.Crags.Find(&api.CragFilter{
		Name:   req.URL.Query().Get("name"),
		Offset: Offset(req),
		Limit:  PaginationLimit,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, crags, Header{"X-Total", n})
}

func (r *router) findCragByID(w http.ResponseWriter, req *http.Request) {
	crag, err := r.Crags.FindByID(GetVar(req, "id"))
	if err != nil {
		if errors.Is(err, pq.ErrRecordNotFound) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		return
	}

	WriteJSON(w, crag)
}

func (r *router) createCrag(w http.ResponseWriter, req *http.Request) {
	crag := &api.Crag{}
	if err := ReadJSON(req, crag); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := r.Crags.Create(crag); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, crag)
}

func (r *router) updateCrag(w http.ResponseWriter, req *http.Request) {
	u := &api.CragUpdates{}
	if err := ReadJSON(req, u); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	crag, err := r.Crags.Update(GetVar(req, "id"), u)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, crag)
}

func (r *router) deleteCrag(w http.ResponseWriter, req *http.Request) {
	if err := r.Crags.Delete(GetVar(req, "id")); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
