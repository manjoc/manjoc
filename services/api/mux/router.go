package mux

import (
	"api"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

const PaginationLimit = 25

type router struct {
	mux *mux.Router

	Crags          api.Crags
	Messages       api.Messages
	Participations api.Participations
	Places         api.Places
	Sessions       api.Sessions
	Users          api.Users
}

func (r *router) Handler() http.Handler {
	return r.mux
}

func NewRouter(
	crags api.Crags,
	messages api.Messages,
	participations api.Participations,
	places api.Places,
	sessions api.Sessions,
	users api.Users,
) *router {
	r := &router{
		mux: mux.NewRouter(),

		Crags:          crags,
		Messages:       messages,
		Participations: participations,
		Places:         places,
		Sessions:       sessions,
		Users:          users,
	}

	// Set Content-Type headers with JSON middleware
	r.mux.Use(JSONMiddleware)

	r.handleCrags()
	r.handleMessages()
	r.handleParticipations()
	r.handlePlaces()
	r.handleSessions()
	r.handleUsers()

	return r
}

func JSONMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if ct := r.Header.Get("Content-Type"); ct != "" && ct != "application/json" {
			http.Error(w, "Unsupported Content-Type header", http.StatusUnsupportedMediaType)
			return
		}

		w.Header().Set("Content-Type", "application/json")

		next.ServeHTTP(w, r)
	})
}

func Offset(r *http.Request) int {
	offset, err := strconv.Atoi(r.URL.Query().Get("offset"))
	if err != nil {
		return 0
	}

	return offset
}

func GetVar(r *http.Request, key string) string {
	return mux.Vars(r)[key]
}

type Header struct {
	Key   string
	Value interface{}
}

func ReadJSON(r *http.Request, v interface{}) error {
	return json.NewDecoder(r.Body).Decode(v)
}

func WriteJSON(w http.ResponseWriter, data interface{}, headers ...Header) {
	for _, h := range headers {
		w.Header().Set(h.Key, fmt.Sprint(h.Value))
	}

	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Printf("[mux] Unable to write JSON (data: %v, error: %s)", data, err)
	}
}
