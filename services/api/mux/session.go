package mux

import (
	"api"
	"api/pq"
	"errors"
	"net/http"
)

func (r *router) handleSessions() {
	r.mux.HandleFunc("/sessions", r.findSessions).Methods("GET")
	r.mux.HandleFunc("/sessions/{id}", r.findSessionByID).Methods("GET")
	r.mux.HandleFunc("/sessions", r.createSession).Methods("POST")
	r.mux.HandleFunc("/sessions/{id}", r.updateSession).Methods("PATCH")
	r.mux.HandleFunc("/sessions/{id}", r.deleteSession).Methods("DELETE")
}

func (r *router) findSessions(w http.ResponseWriter, req *http.Request) {
	sessions, n, err := r.Sessions.Find(&api.SessionFilter{
		Limit:  PaginationLimit,
		Offset: Offset(req),
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, sessions, Header{"X-Total", n})
}

func (r *router) findSessionByID(w http.ResponseWriter, req *http.Request) {
	session, err := r.Sessions.FindByID(GetVar(req, "id"))
	if err != nil {
		if errors.Is(err, pq.ErrRecordNotFound) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		return
	}

	WriteJSON(w, session)
}

func (r *router) createSession(w http.ResponseWriter, req *http.Request) {
	session := &api.Session{}
	if err := ReadJSON(req, session); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := r.Sessions.Create(session); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, session)
}

func (r *router) updateSession(w http.ResponseWriter, req *http.Request) {
	u := &api.SessionUpdates{}
	if err := ReadJSON(req, u); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session, err := r.Sessions.Update(GetVar(req, "id"), u)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WriteJSON(w, session)
}

func (r *router) deleteSession(w http.ResponseWriter, req *http.Request) {
	if err := r.Sessions.Delete(GetVar(req, "id")); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
