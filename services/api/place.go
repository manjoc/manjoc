package api

type Place struct {
	Address string  `json:"address,omitempty"`
	Lat     float64 `json:"lat,omitempty"`
	Lng     float64 `json:"lng,omitempty"`
	Name    string  `json:"name,omitempty"`
}

type Places interface {
	Find(input string) ([]*Place, error)
}
