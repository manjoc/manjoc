package google

// This file contains data types returned by Google Places API. Structures are
// private since they are only needed by this package.

type candidates struct {
	Candidates []*candidate `json:"candidates"`
	Status     string       `json:"status"`
}

// nolint:tagliatelle
type candidate struct {
	FormattedAddress string    `json:"formatted_address"`
	Geometry         *geometry `json:"geometry"`
	Name             string    `json:"name"`
}

type geometry struct {
	Location *location `json:"location"`
}

type location struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}
