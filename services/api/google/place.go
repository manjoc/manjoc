package google

import (
	"api"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/patrickmn/go-cache"
)

type places struct {
	apiKey string
	apiURL string

	cache  *cache.Cache
	client *http.Client
}

func NewPlaces(cache *cache.Cache, apiKey, apiURL string) api.Places {
	return &places{
		apiKey: apiKey,
		apiURL: apiURL,

		cache:  cache,
		client: http.DefaultClient,
	}
}

func (p *places) Find(input string) ([]*api.Place, error) {
	// If available, use cache
	if cachedPlaces, found := p.cache.Get(input); found {
		cachedPlaces, ok := cachedPlaces.([]*api.Place)
		if ok {
			// Postpone cache expiry
			p.cache.Set(input, cachedPlaces, cache.DefaultExpiration)

			return cachedPlaces, nil
		}
	}

	places, status, err := p.find(input)
	if err != nil {
		return nil, err
	} else if status != "OK" {
		return nil, errors.New(
			fmt.Sprintf("Invalid status (%s)", status),
		)
	}

	// Save cache
	p.cache.Set(input, places, cache.DefaultExpiration)

	return places, nil
}

func (p *places) find(input string) ([]*api.Place, string, error) {
	// Build query
	params := url.Values{}
	params.Add("fields", "formatted_address,geometry,name")
	params.Add("input", input)
	params.Add("inputtype", "textquery")
	params.Add("key", p.apiKey)

	// Fetch places
	res, err := p.client.Get(
		fmt.Sprintf("%s?%s", p.apiURL, params.Encode()),
	)
	if err != nil {
		return nil, "", err
	}
	defer res.Body.Close()

	// Format response
	return format(res.Body)
}

func format(body io.Reader) ([]*api.Place, string, error) {
	res := &candidates{}
	if err := json.NewDecoder(body).Decode(&res); err != nil {
		return nil, "", err
	}

	p := []*api.Place{}
	for _, c := range res.Candidates {
		p = append(p, &api.Place{
			Address: c.FormattedAddress,
			Lat:     c.Geometry.Location.Lat,
			Lng:     c.Geometry.Location.Lng,
			Name:    c.Name,
		})
	}

	return p, res.Status, nil
}
