package api

// Message represents a text message posted by a user in a session.
type Message struct {
	Model
	Content  string     `json:"content,omitempty"`
	Session  *Session   `json:"session,omitempty"`
	User     *User      `json:"user,omitempty"`
	Parent   *Message   `json:"parent,omitempty"`
	Children []*Message `json:"children,omitempty"`
}

// MessageFilter represents a filter used to find messages.
type MessageFilter struct {
	ID        string `column:"id"`
	ParentID  string `column:"parent_id"`
	SessionID string `column:"session_id"`

	// Flat loads (or not) nested messages with parent/child relationships
	Flat   bool
	Limit  int
	Offset int
}

// MessageUpdates represents a set of fields that can be updated for a message.
type MessageUpdates struct {
	Content *string
}

// Messages represents a service to manage messages.
type Messages interface {
	Find(filter *MessageFilter) ([]*Message, int, error)
	FindByID(id string) (*Message, error)
	Create(message *Message) error
	Update(id string, u *MessageUpdates) (*Message, error)
	Delete(id string) error
}
