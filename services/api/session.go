package api

import (
	"errors"
	"time"
)

// Session represents a climbing session. A session is attached to a crag and
// users can join it.
type Session struct {
	Model
	Date           *time.Time       `json:"date,omitempty"`
	Private        bool             `json:"private,omitempty"`
	Crag           *Crag            `json:"crag,omitempty"`
	Messages       []*Message       `json:"messages,omitempty"`
	Participations []*Participation `json:"participations,omitempty"`
}

// Validate performs basic formatting and validation.
func (s *Session) Validate() error {
	if s.Date.Before(time.Now()) {
		return errors.New("Date must be in future")
	}

	return nil
}

// SessionFilter represents a filter used to find sessions.
type SessionFilter struct {
	ID     string `column:"sessions.id"`
	CragID string `column:"sessions.crag_id"`
	Limit  int
	Offset int
}

// SessionUpdates represents a set of fields that can be updated for a session.
type SessionUpdates struct {
	Date    *time.Time `json:"date"`
	Private *bool      `json:"private"`
}

// Sessions represents a service to manage sessions.
type Sessions interface {
	Find(filter *SessionFilter) ([]*Session, int, error)
	FindByID(id string) (*Session, error)
	Create(session *Session) error
	Update(id string, u *SessionUpdates) (*Session, error)
	Delete(id string) error
}
