module api

go 1.17

require (
	github.com/joho/godotenv v1.4.0
	golang.org/x/text v0.3.7
)

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.4
)

require (
	github.com/google/uuid v1.3.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
)
