package api

import (
	"regexp"
	"strings"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

func NormalizeString(in *string) error {
	t := transform.Chain(
		norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC,
	)

	out, _, err := transform.String(t, *in)
	if err != nil {
		return err
	}

	out = strings.ToLower(out)

	out = regexp.MustCompile(`[^a-z0-9]`).ReplaceAllString(out, "")

	*in = out

	return nil
}
