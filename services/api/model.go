package api

import (
	"time"
)

// Model represents a minimal structure used by most domain objects.
type Model struct {
	ID        string     `json:"id"`
	CreatedAt *time.Time `json:"createdAt,omitempty"`
	UpdatedAt *time.Time `json:"updatedAt,omitempty"`
}
