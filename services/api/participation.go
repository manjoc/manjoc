package api

import (
	"time"
)

// ParticipationStatus represents the state of a participation.
type ParticipationStatus string

func (s ParticipationStatus) String() string {
	return string(s)
}

const (
	ParticipationStatusUnknown  = "unknown"
	ParticipationStatusOwner    = "owner"
	ParticipationStatusAccepted = "accepted"
	ParticipationStatusRefused  = "refused"
)

// Participation represents a link between a user and a session.
type Participation struct {
	Session   *Session            `json:"session,omitempty"`
	User      *User               `json:"user,omitempty"`
	CreatedAt *time.Time          `json:"createdAt,omitempty"`
	UpdatedAt *time.Time          `json:"updatedAt,omitempty"`
	Status    ParticipationStatus `json:"status,omitempty"`
}

// ParticipationFilter represents a filter used to find participations.
type ParticipationFilter struct {
	SessionID string `column:"session_id"`
	UserID    string `column:"user_id"`
	Limit     int
	Offset    int
}

// ParticipationUpdates represents a set of fields that can be updated for a
// participation.
type ParticipationUpdates struct {
	Status *ParticipationStatus
}

// Participations represents a service to manage participations.
type Participations interface {
	Find(filter *ParticipationFilter) ([]*Participation, int, error)
	FindByIDs(sessionID, userID string) (*Participation, error)
	Create(participation *Participation) error
	Update(sessionID, userID string, u *ParticipationUpdates) (*Participation, error)
	Delete(sessionID, userID string) error
}
