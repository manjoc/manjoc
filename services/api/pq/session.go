package pq

import (
	"api"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
)

// sessions implements Sessions interface with postgres database.
type sessions struct {
	db *sql.DB
}

func NewSessions(db *sql.DB) api.Sessions {
	return &sessions{
		db: db,
	}
}

func (s *sessions) Find(filter *api.SessionFilter) ([]*api.Session, int, error) {
	return findSessions(s.db, filter)
}

func (s *sessions) FindByID(id string) (*api.Session, error) {
	session, err := findSessionByID(s.db, id)
	if err != nil {
		return nil, err
	} else if err := attachSessionMessages(s.db, session); err != nil {
		return nil, err
	} else if err := attachSessionParticipations(s.db, session); err != nil {
		return nil, err
	}

	return session, nil
}

func (s *sessions) Create(session *api.Session) error {
	return createSession(s.db, session)
}

func (s *sessions) Update(id string, u *api.SessionUpdates) (*api.Session, error) {
	return updateSession(s.db, id, u)
}

func (s *sessions) Delete(id string) error {
	return deleteSession(s.db, id)
}

func findSessions(db *sql.DB, filter *api.SessionFilter) (_ []*api.Session, n int, _ error) {
	// Execute query & load related crag
	rows, err := db.Query(fmt.Sprintf(`
    SELECT
      sessions.id,
      sessions.date,
      sessions.private,
      sessions.created_at,
      sessions.updated_at,
      crags.id,
      crags.name,
      crags.lat,
      crags.lng,
      COUNT(*) OVER()
    FROM sessions
    LEFT JOIN crags ON sessions.crag_id = crags.id
    WHERE %s
    ORDER BY date ASC %s
  `, Where(filter), Surround(filter.Limit, filter.Offset)))
	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	// Deserialize rows into sessions
	sessions := []*api.Session{}

	for rows.Next() {
		crag, session := api.Crag{}, api.Session{}

		if err := rows.Scan(
			&session.ID,
			&session.Date,
			&session.Private,
			&session.CreatedAt,
			&session.UpdatedAt,
			&crag.ID,
			&crag.Name,
			&crag.Lat,
			&crag.Lng,
			&n,
		); err != nil {
			return nil, 0, err
		}

		session.Crag = &crag

		sessions = append(sessions, &session)
	}

	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return sessions, n, nil
}

func findSessionByID(db *sql.DB, id string) (*api.Session, error) {
	sessions, n, err := findSessions(db, &api.SessionFilter{ID: id})
	if err != nil {
		return nil, err
	} else if n == 0 {
		return nil, ErrRecordNotFound
	}

	return sessions[0], nil
}

func createSession(db *sql.DB, s *api.Session) error {
	// Set ID
	s.ID = uuid.NewString()

	// Set timestamps
	s.CreatedAt = Now()
	s.UpdatedAt = s.CreatedAt

	// Validate model
	if err := s.Validate(); err != nil {
		return err
	}

	// Execute query
	if _, err := db.Exec(`
    INSERT INTO sessions (id, date, private, crag_id, created_at, updated_at)
    VALUES ($1, $2, $3, $4, $5, $6)
  `, s.ID, s.Date, s.Private, s.Crag.ID, s.CreatedAt, s.UpdatedAt,
	); err != nil {
		return err
	}

	return nil
}

func updateSession(db *sql.DB, id string, u *api.SessionUpdates) (*api.Session, error) {
	// Fetch existing session
	s, err := findSessionByID(db, id)
	if err != nil {
		return nil, err
	}

	// Update fields
	if v := u.Date; v != nil {
		date := *v
		s.Date = &date
	}

	if v := u.Private; v != nil {
		s.Private = *v
	}

	s.UpdatedAt = Now()

	// Validate model
	if err := s.Validate(); err != nil {
		return nil, err
	}

	// Execute query
	if _, err := db.Exec(`
    UPDATE sessions
    SET date = $1, private = $2, updated_at = $3
    WHERE id = $4
  `, s.Date, s.Private, s.UpdatedAt, id); err != nil {
		return nil, err
	}

	return s, nil
}

func deleteSession(db *sql.DB, id string) error {
	// Session should exist
	if _, err := findSessionByID(db, id); err != nil {
		return err
	}

	// Remove from database
	if _, err := db.Exec(`DELETE FROM sessions WHERE id = $1`, id); err != nil {
		return err
	}

	return nil
}

func attachSessionMessages(db *sql.DB, s *api.Session) (err error) {
	if s.Messages, _, err = findMessages(db, &api.MessageFilter{
		SessionID: s.ID,
	}); err != nil {
		return err
	}

	return nil
}

func attachSessionParticipations(db *sql.DB, s *api.Session) (err error) {
	if s.Participations, _, err = findParticipations(db, &api.ParticipationFilter{
		SessionID: s.ID,
	}); err != nil {
		return err
	}

	return nil
}
