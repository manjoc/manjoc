package pq

import (
	"database/sql"
	"fmt"
	"reflect"
	"time"

	_ "github.com/lib/pq"
)

// Config stores information to connect to the database.
type Config struct {
	Host     string
	Port     string
	Database string
	User     string
	Password string
}

// Open opens a connection with the database.
func Open(c *Config) (*sql.DB, error) {
	// Build Data Source Name (DSN)
	dsn := fmt.Sprintf(
		"dbname=%s host=%s password=%s port=%s user=%s sslmode=disable",
		c.Database,
		c.Host,
		c.Password,
		c.Port,
		c.User,
	)

	// Open connection
	return sql.Open("postgres", dsn)
}

func Now() *time.Time {
	now := time.Now()
	return &now
}

// Where builds a WHERE SQL clause based on given filter. Use `column:""` field
// tag in the filter to add it to the clause. The value of the tag should match
// the name of the column in the database (e.g. `column:"user_id"`).
func Where(filter interface{}) string {
	// Start with an always true predicate. Thus, "SELECT * FROM x WHERE 1 = 1" is
	// equivalent to "SELECT * FROM x".
	where := "1 = 1"

	v := reflect.ValueOf(filter)

	// Follow pointers to work with underlying element
	for v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	// This function only works with structures
	if v.Kind() != reflect.Struct {
		return where
	}

	// Browse fields
	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)

		// Skip missing fields
		if field.IsZero() {
			continue
		}

		if tag := v.Type().Field(i).Tag.Get("column"); tag != "" {
			// Complete clause
			where = fmt.Sprintf(
				`%s AND %s = '%s'`, where, tag, field.String(),
			)
		}
	}

	return where
}

// Surround builds a string with LIMIT and OFFSET SQL clauses. A clause is added
// only if the corresponding parameter is greater than zero.
func Surround(limit, offset int) string {
	if limit > 0 && offset > 0 {
		return fmt.Sprintf("LIMIT %d OFFSET %d", limit, offset)
	} else if limit > 0 {
		return fmt.Sprintf("LIMIT %d", limit)
	} else if offset > 0 {
		return fmt.Sprintf("OFFSET %d", offset)
	}

	return ""
}
