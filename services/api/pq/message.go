package pq

import (
	"api"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
)

// messages implements Messages interface with postgres database.
type messages struct {
	db *sql.DB
}

func NewMessages(db *sql.DB) api.Messages {
	return &messages{
		db: db,
	}
}

func (m *messages) Find(filter *api.MessageFilter) ([]*api.Message, int, error) {
	return findMessages(m.db, filter)
}

func (m *messages) FindByID(id string) (*api.Message, error) {
	return findMessageByID(m.db, id)
}

func (m *messages) Create(message *api.Message) error {
	return createMessage(m.db, message)
}

func (m *messages) Update(id string, u *api.MessageUpdates) (*api.Message, error) {
	return updateMessage(m.db, id, u)
}

func (m *messages) Delete(id string) error {
	return deleteMessage(m.db, id)
}

func findMessages(db *sql.DB, filter *api.MessageFilter) (_ []*api.Message, n int, _ error) {
	where := Where(filter)
	if !filter.Flat {
		// Only fetch parents
		where = fmt.Sprintf("%s AND parent_id IS NULL", where)
	}

	// Execute query
	rows, err := db.Query(fmt.Sprintf(`
    SELECT
      id,
      content,
      session_id,
      user_id,
      created_at,
      updated_at,
      COUNT(*) OVER()
    FROM messages
    WHERE %s
    ORDER BY created_at ASC %s
  `, where, Surround(filter.Limit, filter.Offset)))
	if err != nil {
		return nil, 0, err
	}

	defer rows.Close()

	// Deserialize rows into messages
	messages := []*api.Message{}

	for rows.Next() {
		message, session, user := api.Message{}, api.Session{}, api.User{}

		if rows.Scan(
			&message.ID,
			&message.Content,
			&session.ID,
			&user.ID,
			&message.CreatedAt,
			&message.UpdatedAt,
			&n,
		); err != nil {
			return nil, 0, err
		}

		message.Session, message.User = &session, &user

		if !filter.Flat {
			// Fetch children
			attachMessageChildren(db, &message)
		}

		messages = append(messages, &message)
	}

	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return messages, n, nil
}

func findMessageByID(db *sql.DB, id string) (*api.Message, error) {
	messages, n, err := findMessages(db, &api.MessageFilter{ID: id})
	if err != nil {
		return nil, err
	} else if n == 0 {
		return nil, ErrRecordNotFound
	}

	return messages[0], nil
}

func createMessage(db *sql.DB, m *api.Message) error {
	// Set ID
	m.ID = uuid.NewString()

	// Set timestamps
	m.CreatedAt = Now()
	m.UpdatedAt = m.CreatedAt

	parentID := interface{}(nil)
	if m.Parent != nil && m.Parent.ID != "" {
		parentID = m.Parent.ID
	}

	// Execute query
	if _, err := db.Exec(`
    INSERT INTO messages (id, content, parent_id, session_id, user_id, created_at, updated_at)
    VALUES ($1, $2, $3, $4, $5, $6, $7)
  `, m.ID, m.Content, parentID, m.Session.ID, m.User.ID, m.CreatedAt, m.UpdatedAt,
	); err != nil {
		return err
	}

	return nil
}

func updateMessage(db *sql.DB, id string, u *api.MessageUpdates) (*api.Message, error) {
	// Fetch existing message
	m, err := findMessageByID(db, id)
	if err != nil {
		return nil, err
	}

	// Update fields
	if v := u.Content; v != nil {
		m.Content = *v
	}

	m.UpdatedAt = Now()

	// Execute query
	if _, err := db.Exec(`
    UPDATE messages
    SET content = $1, updated_at = $2
    WHERE id = $3
  `, m.Content, m.UpdatedAt, id); err != nil {
		return nil, err
	}

	return m, nil
}

func deleteMessage(db *sql.DB, id string) error {
	// Message should exist
	if _, err := findMessageByID(db, id); err != nil {
		return err
	}

	// Remove from database
	if _, err := db.Exec(`DELETE FROM messages WHERE id = $1`, id); err != nil {
		return err
	}

	return nil
}

func attachMessageChildren(db *sql.DB, m *api.Message) (err error) {
	if m.Children, _, err = findMessages(db, &api.MessageFilter{
		Flat:     true,
		ParentID: m.ID,
	}); err != nil {
		return err
	}

	return nil
}
