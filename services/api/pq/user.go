package pq

import (
	"api"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
)

// users implements Users interface with postgres database.
type users struct {
	db *sql.DB
}

func NewUsers(db *sql.DB) api.Users {
	return &users{
		db: db,
	}
}

func (u *users) Find(filter *api.UserFilter) ([]*api.User, int, error) {
	return findUsers(u.db, filter)
}

func (u *users) FindByID(id string) (*api.User, error) {
	user, err := findUserByID(u.db, id)
	if err != nil {
		return nil, err
	} else if err := attachUserParticipations(u.db, user); err != nil {
		return nil, err
	}

	return user, nil
}

func (u *users) Create(user *api.User) error {
	return createUser(u.db, user)
}

func (u *users) Update(id string, upd *api.UserUpdates) (*api.User, error) {
	return updateUser(u.db, id, upd)
}

func (u *users) Delete(id string) error {
	return deleteUser(u.db, id)
}

func findUsers(db *sql.DB, filter *api.UserFilter) (_ []*api.User, n int, _ error) {
	// Execute query
	rows, err := db.Query(fmt.Sprintf(`
    SELECT id, avatar, eight_a, email, full_name, created_at, updated_at, COUNT(*) OVER()
    FROM users
    WHERE %s
    ORDER BY email ASC %s
  `, Where(filter), Surround(filter.Limit, filter.Offset)))
	if err != nil {
		return nil, 0, err
	}

	defer rows.Close()

	// Deserialize rows into users
	users := []*api.User{}

	for rows.Next() {
		var user api.User

		if rows.Scan(
			&user.ID,
			&user.Avatar,
			&user.EightA,
			&user.Email,
			&user.FullName,
			&user.CreatedAt,
			&user.UpdatedAt,
			&n,
		); err != nil {
			return nil, 0, err
		}

		users = append(users, &user)
	}

	return users, n, nil
}

func findUserByID(db *sql.DB, id string) (*api.User, error) {
	users, n, err := findUsers(db, &api.UserFilter{ID: id})
	if err != nil {
		return nil, err
	} else if n == 0 {
		return nil, ErrRecordNotFound
	}

	return users[0], nil
}

func createUser(db *sql.DB, u *api.User) error {
	// Set ID
	u.ID = uuid.NewString()

	// Set timestamps
	u.CreatedAt = Now()
	u.UpdatedAt = u.CreatedAt

	// Validate model
	if err := u.Validate(); err != nil {
		return err
	}

	// Execute query
	if _, err := db.Exec(`
    INSERT INTO users (id, avatar, eight_a, email, full_name, created_at, updated_at)
    VALUES ($1, $2, $3, $4, $5, $6, $7)
  `, u.ID, u.Avatar, u.EightA, u.Email, u.FullName, u.CreatedAt, u.UpdatedAt,
	); err != nil {
		return err
	}

	return nil
}

func updateUser(
	db *sql.DB, id string, upd *api.UserUpdates,
) (*api.User, error) {
	// Fetch existing user
	u, err := findUserByID(db, id)
	if err != nil {
		return nil, err
	}

	// Update fields
	if v := upd.Avatar; v != nil {
		u.Avatar = *v
	}

	if v := upd.EightA; v != nil {
		u.EightA = *v
	}

	if v := upd.Email; v != nil {
		u.Email = *v
	}

	if v := upd.FullName; v != nil {
		u.FullName = *v
	}

	u.UpdatedAt = Now()

	// Validate model
	if err := u.Validate(); err != nil {
		return nil, err
	}

	// Execute query
	if _, err := db.Exec(`
    UPDATE users
    SET avatar = $1, eight_a = $2, email = $3, full_name = $4, updated_at = $5
    WHERE id = $6
  `, u.Avatar, u.EightA, u.Email, u.FullName, u.UpdatedAt, id); err != nil {
		return nil, err
	}

	return u, nil
}

func deleteUser(db *sql.DB, id string) error {
	// User should exist
	if _, err := findUserByID(db, id); err != nil {
		return err
	}

	// Remove from database
	if _, err := db.Exec(`DELETE FROM users WHERE id = $1`, id); err != nil {
		return err
	}

	return nil
}

func attachUserParticipations(db *sql.DB, u *api.User) (err error) {
	if u.Participations, _, err = findParticipations(db, &api.ParticipationFilter{
		UserID: u.ID,
	}); err != nil {
		return err
	}

	return nil
}
