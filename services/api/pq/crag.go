package pq

import (
	"api"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
)

// crags implements Crags interface with postgres database.
type crags struct {
	db *sql.DB
}

func NewCrags(db *sql.DB) api.Crags {
	return &crags{
		db: db,
	}
}

func (c *crags) Find(filter *api.CragFilter) ([]*api.Crag, int, error) {
	return findCrags(c.db, filter)
}

func (c *crags) FindByID(id string) (*api.Crag, error) {
	return findCragByID(c.db, id)
}

func (c *crags) Create(crag *api.Crag) error {
	return createCrag(c.db, crag)
}

func (c *crags) Update(id string, u *api.CragUpdates) (*api.Crag, error) {
	return updateCrag(c.db, id, u)
}

func (c *crags) Delete(id string) error {
	return deleteCrag(c.db, id)
}

func findCrags(db *sql.DB, filter *api.CragFilter) (_ []*api.Crag, n int, _ error) {
	// Execute query
	rows, err := db.Query(fmt.Sprintf(`
    SELECT
      id,
      name,
      slug,
      lat,
      lng,
      created_at,
      updated_at,
      COUNT(*) OVER()
    FROM crags
    WHERE %s
    ORDER BY name ASC %s
  `, Where(filter), Surround(filter.Limit, filter.Offset)))
	if err != nil {
		return nil, 0, err
	}

	defer rows.Close()

	// Deserialize rows into crags
	crags := []*api.Crag{}

	for rows.Next() {
		var crag api.Crag

		if rows.Scan(
			&crag.ID,
			&crag.Name,
			&crag.Slug,
			&crag.Lat,
			&crag.Lng,
			&crag.CreatedAt,
			&crag.UpdatedAt,
			&n,
		); err != nil {
			return nil, 0, err
		}

		crags = append(crags, &crag)
	}

	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return crags, n, nil
}

func findCragByID(db *sql.DB, id string) (*api.Crag, error) {
	crags, n, err := findCrags(db, &api.CragFilter{ID: id})
	if err != nil {
		return nil, err
	} else if n == 0 {
		return nil, ErrRecordNotFound
	}

	return crags[0], nil
}

func createCrag(db *sql.DB, c *api.Crag) error {
	// Set ID
	c.ID = uuid.NewString()

	// Set timestamps
	c.CreatedAt = Now()
	c.UpdatedAt = c.CreatedAt

	// Validate model
	if err := c.Validate(); err != nil {
		return err
	}

	// Execute query
	if _, err := db.Exec(`
    INSERT INTO crags (id, name, slug, lat, lng, created_at, updated_at)
    VALUES ($1, $2, $3, $4, $5, $6, $7)
  `, c.ID, c.Name, c.Slug, c.Lat, c.Lng, c.CreatedAt, c.UpdatedAt,
	); err != nil {
		return err
	}

	return nil
}

func updateCrag(db *sql.DB, id string, u *api.CragUpdates) (*api.Crag, error) {
	// Fetch existing crag
	c, err := findCragByID(db, id)
	if err != nil {
		return nil, err
	}

	// Update fields
	if v := u.Name; v != nil {
		c.Name = *v
	}

	if v := u.Lat; v != nil {
		c.Lat = *v
	}

	if v := u.Lng; v != nil {
		c.Lng = *v
	}

	c.UpdatedAt = Now()

	// Validate model
	if err := c.Validate(); err != nil {
		return nil, err
	}

	// Execute query
	if _, err := db.Exec(`
    UPDATE crags
    SET name = $1, lat = $2, lng = $3, updated_at = $4
    WHERE id = $5
  `, c.Name, c.Lat, c.Lng, c.UpdatedAt, id); err != nil {
		return nil, err
	}

	return c, nil
}

func deleteCrag(db *sql.DB, id string) error {
	// Crag should exist
	if _, err := findCragByID(db, id); err != nil {
		return err
	}

	// Remove from database
	if _, err := db.Exec(`DELETE FROM crags WHERE id = $1`, id); err != nil {
		return err
	}

	return nil
}
