package pq

import "errors"

// Pre-defined errors.
var (
	ErrRecordNotFound = errors.New("Record not found")
)
