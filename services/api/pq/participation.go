package pq

import (
	"api"
	"database/sql"
	"fmt"
)

// participations implements Participations interface with postgres database.
type participations struct {
	db *sql.DB
}

func NewParticipations(db *sql.DB) api.Participations {
	return &participations{
		db: db,
	}
}

func (p *participations) Find(filter *api.ParticipationFilter) ([]*api.Participation, int, error) {
	return findParticipations(p.db, filter)
}

func (p *participations) FindByIDs(sessionID, userID string) (*api.Participation, error) {
	return findParticipation(p.db, sessionID, userID)
}

func (p *participations) Create(participation *api.Participation) error {
	return createParticipation(p.db, participation)
}

func (p *participations) Update(sessionID, userID string, u *api.ParticipationUpdates) (*api.Participation, error) {
	return updateParticipation(p.db, sessionID, userID, u)
}

func (p *participations) Delete(sessionID, userID string) error {
	return deleteParticipation(p.db, sessionID, userID)
}

func findParticipations(db *sql.DB, filter *api.ParticipationFilter) (_ []*api.Participation, n int, _ error) {
	// Execute query & load related information
	rows, err := db.Query(fmt.Sprintf(`
    SELECT
      participations.status,
      participations.created_at,
      participations.updated_at,
      sessions.id,
      users.id,
      users.avatar,
      users.full_name,
      COUNT(*) OVER()
    FROM participations
    LEFT JOIN sessions ON participations.session_id = sessions.id
    LEFT JOIN users ON participations.user_id = users.id
    WHERE %s
    ORDER BY created_at ASC %s
  `, Where(filter), Surround(filter.Limit, filter.Offset)))
	if err != nil {
		return nil, 0, err
	}

	defer rows.Close()

	// Deserialize rows into participations
	participations := []*api.Participation{}

	for rows.Next() {
		participation, session, user := api.Participation{}, api.Session{}, api.User{}

		if rows.Scan(
			&participation.Status,
			&participation.CreatedAt,
			&participation.UpdatedAt,
			&session.ID,
			&user.ID,
			&user.Avatar,
			&user.FullName,
			&n,
		); err != nil {
			return nil, 0, err
		}

		participation.Session, participation.User = &session, &user

		participations = append(participations, &participation)
	}

	return participations, n, nil
}

func findParticipation(db *sql.DB, sessionID, userID string) (*api.Participation, error) {
	// Fetch existing participation
	participations, n, err := findParticipations(db, &api.ParticipationFilter{
		SessionID: sessionID,
		UserID:    userID,
	})
	if err != nil {
		return nil, err
	} else if n == 0 {
		return nil, ErrRecordNotFound
	}

	return participations[0], nil
}

func createParticipation(db *sql.DB, p *api.Participation) error {
	// Set timestamps
	p.CreatedAt = Now()
	p.UpdatedAt = p.CreatedAt

	// Execute query
	if _, err := db.Exec(`
    INSERT INTO participations (session_id, user_id, status, created_at, updated_at)
    VALUES ($1, $2, $3, $4, $5)
  `, p.Session.ID, p.User.ID, p.Status, p.CreatedAt, p.UpdatedAt,
	); err != nil {
		return err
	}

	return nil
}

func updateParticipation(
	db *sql.DB, sessionID, userID string, u *api.ParticipationUpdates,
) (*api.Participation, error) {
	// Fetch existing participation
	p, err := findParticipation(db, sessionID, userID)
	if err != nil {
		return nil, err
	}

	// Update fields
	if v := u.Status; v != nil {
		p.Status = *v
	}

	p.UpdatedAt = Now()

	// Execute query
	if _, err := db.Exec(`
    UPDATE participations
    SET status = $1, updated_at = $2
    WHERE session_id = $3 AND user_id = $4
  `, p.Status, p.UpdatedAt, sessionID, userID); err != nil {
		return nil, err
	}

	return p, nil
}

func deleteParticipation(db *sql.DB, sessionID, userID string) error {
	// Participation should exist
	if _, err := findParticipation(db, sessionID, userID); err != nil {
		return err
	}

	// Remove from database
	if _, err := db.Exec(`
    DELETE FROM participations WHERE session_id = $1 AND user_id = $2
  `, sessionID, userID); err != nil {
		return err
	}

	return nil
}
